(function ($) {

    $(function () {
        // date picker
        add_datepicker();
        function add_datepicker(){
            $("body").on('focus','input.date_picker.joined', function(){
                $(this).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    onClose: function (selectedDate) {
                        $(this).parents(".education-info").find("input.date_picker.completed").datepicker({
                            minDate: selectedDate
                        })
                    }
                });
            });
            $("body").on('focus','input.date_picker.completed', function(){
                $(this).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    onClose: function (selectedDate) {
                        $(this).parents(".education-info").find("input.date_picker.joined").datepicker({
                            maxDate: selectedDate
                        })
                    }
                });
            });
        }

        var counter = 0;
        $("button#add_education").click(function (e) {
            var siblings = $(this).siblings("div.education-info");
            // for each child present add to the counter
            counter = counter+siblings.length;

            // add one to the current counter value either there is a sibling or not
            counter++;

            var school_id = counter;
            var school_info =
                '<div class="education-info">'+
                    '<div class="form-group">'+
                        '<label for="school">School</label>'+
                        '<input type="text" name="schools['+school_id+'][name]" id="school"/>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="certificate_awarded">certificate_awarded</label>'+
                        '<input type="text" name="schools['+school_id+'][certificate_awarded]" id="certificate_awarded"/>'+
                    '</div>'+
                    '<div class="form-group small">'+
                        '<label for="joined">Joined</label>'+
                        '<input type="text" class="date_picker joined" name="schools['+school_id+'][joined]"/>'+
                    '</div>'+
                    '<div class="form-group small">'+
                        '<label for="completed">Completed</label>'+
                        '<input type="text" class="date_picker completed" name="schools['+school_id+'][completed]"/>'+
                    '</div>'+
                    '<div class="form-group small">'+
                        '<label for="grade">grade</label>'+
                        '<input type="text" id="grade" name="schools['+school_id+'][grade]"/>'+
                    '</div>'+
                '</div>';

            $(this).after(school_info);

            // date picker
            add_datepicker();
        });

        var skill_counter = 0;
        $("button#add_skills").click(function (e) {
            var siblings = $(this).siblings("div.education-info");
            skill_counter = skill_counter+siblings.length;

            // add one to the current counter value either there is a sibling or not
            skill_counter++;
            console.log(skill_counter);

            var skill_id = skill_counter;
            var skills_info =
                '<div class="education-info" >'+
                    '<div class="form-group small">'+
                        '<label for="skill_name">skill</label>'+
                        '<input type="text" name="skills['+skill_id+'][name]"/>'+
                    '</div>'+
                    '<div class="form-group small">'+
                        '<label for="skill_level">skill level</label>'+
                        '<select name="skills['+skill_id+'][level]" id="skill-level">'+
                            '<option value="1">1</option>'+
                            '<option value="2">2</option>'+
                            '<option value="3">3</option>'+
                            '<option value="4">4</option>'+
                            '<option value="5">5</option>'+
                            '<option value="6">6</option>'+
                            '<option value="7">7</option>'+
                            '<option value="8">8</option>'+
                            '<option value="9">9</option>'+
                            '<option value="10">10</option>'+
                        '</select>'+
                    '</div>'+
                '</div>';
            $(this).after(skills_info);
        });
    });

})(jQuery);

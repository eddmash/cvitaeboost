<?php
/**
Plugin Name: cvitaeboost
Plugin URI: http://eddmash.com/projects/cvitaeboost
Description: Adds curriculum vitae options like education, skills and projects done to a theme
Author: Eddilbert Macharia
Version: 1.0
Author URI: http://eddmash.com
 */


function css_js(){
    wp_enqueue_style('boostcss', plugin_dir_url(__FILE__)."boost.css");
    wp_enqueue_style('jqueryuicss', "http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css");
    wp_enqueue_script('boostjs', plugin_dir_url(__FILE__)."boost.js", array('jquery','jquery-ui-datepicker') );
}
add_action( 'wp_enqueue_scripts', 'css_js' );
add_action( 'admin_enqueue_scripts', 'css_js' );



include("projects/projects.php");
include("education/education.php");
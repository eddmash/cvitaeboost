<?php
$education_fields = array("college", "joined", "completed", "certificate", "grade");
$skills = array("skill", "skill_level");
$buttons = array("add_education", "add_skills");

function create_education_type() {
    register_post_type( 'cv_education',
        array(
            'labels' => array(
                'name' => __( 'Educations', 'cvitaeboost'),
                'singular_name' => __( 'Education', 'cvitaeboost')
            ),
            'public' => true,
            'has_archive' => true,
            'capability_type'    => 'page',
            'menu_position' => 6,
            'menu_icon'=>'dashicons-welcome-learn-more',
            'supports'=>'title',
            'rewrite' => array('slug' => 'education'),
        )
    );
}
add_action( 'init', 'create_education_type' );

/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
function create_education_fields() {
    global $education_fields;
    global $buttons;
    $screens = array( 'cv_education' );
    foreach($buttons as $button){
        foreach ( $screens as $screen ) {
            add_meta_box(
                $button,
                sprintf(__( "%1s", 'cvitaeboost' ), $button),
                $button.'_html',
                $screen,
                'normal',
                'high'
            );
        }

    }
}
add_action( 'add_meta_boxes', 'create_education_fields' );

function add_education_html($post, $btn){

    echo "<button type='button' id='".$btn['id']."'>".$btn['id']."</button>";
    // Add a nonce field so we can check for it later, its like a csrf token
    wp_nonce_field( 'save_schools', 'schools_nonce');

    /*
     * Use get_post_meta() to retrieve an existing value
     * from the database and use the value for the form.
     */
    $schools = get_post_meta( $post->ID, 'schools', true );
    if($schools){
        // unserialize the value so that we get the data as an array
        $schools = unserialize($schools);

        foreach($schools as $key=>$school){?>
            <div class="education-info" school_id="<?php echo $key?>">
                <div class="form-group">
                    <label for="school">School</label>
                    <input type="text" name="schools[<?php echo $key?>][name]"
                           id="school" value="<?php echo $school['name'];?>"/>
                </div>
                <div class="form-group">
                    <label for="certificate_awarded">School</label>
                    <input type="text" name="schools[<?php echo $key?>][certificate_awarded]"
                           id="certificate_awarded" value="<?php echo $school['certificate_awarded'];?>"/>
                </div>
                <div class="form-group small">
                    <label for="joined">Joined</label>
                    <input type="text" class="date_picker joined" name="schools[<?php echo $key?>][joined]"
                           value="<?php echo $school['joined'];?>"/>
                </div>
                <div class="form-group small">
                    <label for="completed">Completed</label>
                    <input type="text" class="date_picker completed" name="schools[<?php echo $key?>][completed]"
                           value="<?php echo $school['completed'];?>"/>
                </div>
                <div class="form-group small">
                    <label for="grade">grade</label>
                    <input type="text" id="grade" name="schools[<?php echo $key?>][grade]"
                           value="<?php echo $school['grade'];?>"/>
                </div>
            </div>
 <?php
        }//close foreach loop
    } // close if statement
}// add_education_html function

function add_skills_html($post, $btn){
    echo "<button type='button' id='".$btn['id']."'>".$btn['id']."</button>";
    // Add a nonce field so we can check for it later, its like a csrf token
    wp_nonce_field( 'save_skills', 'skills_nonce');

    /*
     * Use get_post_meta() to retrieve an existing value
     * from the database and use the value for the form.
     */
    $skills = get_post_meta( $post->ID, 'skills', true );

    if($skills){
        // unserialize the value so that we get the data as an array
        $skills = unserialize($skills);

        foreach($skills as $key=>$skills){?>

            <div class="education-info" >
                <div class="form-group small">
                    <label for="skill_name">skill</label>
                    <input type="text" name="skills[<?php echo $key?>][name]"
                           value="<?php echo $skills['name'];?>"/>
                </div>
                <div class="form-group small">
                    <label for="skill_level">skill level</label>
                    <select name="skills[<?php echo $key?>][level]" id="skill_level">
                        <?php
                        for($i=1; $i<=10; $i++){
                            if($skills['level']==$i){
                                echo '<option value="'.$i.'" selected>'.$i.'</option>';

                            }else{
                                echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>


<?php }
    }
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function save_education_data($post_id){
    // check nonce

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    if ( ! current_user_can( 'edit_page', $post_id ) ) {
        return;
    }
    // Check the user's permissions.
    if(isset($_POST['schools'])){
//        foreach($_POST['schools'] as $school){
//
//            $string = sanitize_text_field($string);
//        }
        $string = serialize($_POST['schools']);
        // serialize the array of schools provided by user
        update_post_meta( $post_id, 'schools', $string);
    }

    if(isset($_POST['skills'])){
        $string = serialize($_POST['skills']);
//        $string = sanitize_text_field($string);
        // serialize the array of schools provided by user
        update_post_meta( $post_id, 'skills', $string);
    }
}
add_action('save_post', 'save_education_data');


function single_education($single_template){
    global $post;
    if ($post->post_type == 'cv_education') {
        $single_template = dirname( __FILE__ ).'/single-cv_education.php';
    }
    return $single_template;
}
add_filter('single_template','single_education');

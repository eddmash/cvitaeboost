<?php get_header();?>

<div class="container">
    <?php get_template_part("inc/menu");?>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-12 ">
            <div class="boost-content">
                <!--            The loop -->
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div <?php post_class(); ?> >

                        <?php if(get_edit_post_link()){?>
                            <small class="pull-right edit_post_link">
                                <a href="<?php echo get_edit_post_link()?>">
                                    <i class="fa fa-edit fg_teal"></i> Edit
                                </a>
                            </small>
                        <?php } ?>
                        <!-- ********************  SCHOOLS ************************ -->
                        <?php $schools = get_post_meta(get_the_ID(), 'schools', true );
                        if($schools){ $schools = unserialize($schools);?>
                            <div class="schools">
                                <h4 class="schools-title"><?php _e("Education", "cvitaeboost");?></h4>

                                <ul>
                                    <?php foreach($schools as $school){ ?>
                                        <li class="school">
                                        <span class="college">
                                             <strong class="title">School:&nbsp;</strong>
                                            <?php  esc_html_e($school['name'], "cvitaeboost");  ?>
                                        </span>

                                        <span class="school-years">
                                            <strong class="title">Attended:&nbsp;</strong>
                                            <span class="date-joined">
                                                <?php esc_html_e($school['joined'], "cvitaeboost");?>
                                            </span>
                                            <span class="years-separator">&#45;</span>

                                            <span class="date-completed">
                                                <?php esc_html_e($school['completed'], "cvitaeboost");?>
                                            </span>
                                        </span>
                                        <span class="certificate">
                                            <strong class="title">Certificate Awarded:&nbsp;</strong>

                                            <?php esc_html_e($school['certificate_awarded'], "cvitaeboost");?>
                                        </span>
                                            <?php if($school['grade']){?>
                                                <span class="grade">
                                                    <strong class="title">Grade Awarded:&nbsp;</strong>
                                                        <?php esc_html_e($school['grade'], "cvitaeboost");?>
                                                </span>
                                            <?php }?>
                                        </li>
                                    <?php }?>
                                </ul>
                            </div>
                        <?php }?>
                        <!-- ********************  SKILLS ************************ -->
                        <?php $skills = get_post_meta(get_the_ID(), 'skills', true );
                        if($skills){$skills = unserialize($skills);?>
                            <div class="skills">
                                <h4 class="skills-title"><?php esc_html_e("Skills", "cvitaeboost");?></h4>
                                <ul>
                                    <?php foreach($skills as $skill){ ?>
                                        <li class="skill">
                                        <span class="skill-name">
                                            <?php esc_html_e($skill['name'], "cvitaeboost"); ?>
                                        </span>
                                        <span class="skill-level">
                                            <?php
                                            $skill_level = $skill['level'];
                                            $remaining = 10-$skill_level;
                                            for($i=1; $i<=$skill_level; $i++){
                                                echo '<i class="fa fa-star"></i>';
                                            }
                                            for($i=1; $i<=$remaining; $i++){
                                                echo '<i class="fa fa-star-o"></i>';
                                            }
                                            ?>


                                        </span>
                                        </li>
                                    <?php } ?>
                                </ul>

                            </div>
                        <?php } ?>
                    </div>
                <?php endwhile;?>
                    <!--                    After all the posts have been looped-->
                    <?php get_template_part("inc/pagination")?>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'cvitae'); ?></p>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-sm-2">
            <?php get_template_part("inc/right_sidebar");?>
        </div>
    </div>
</div>


<?php get_footer();?>
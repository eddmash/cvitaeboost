<?php get_header();?>

<div class="container">
    <?php get_template_part("inc/menu");?>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
            <div  class="boost-content">
                <?php if ( have_posts() ) : ?>
                    <ul class="projects">
                        <!--            The loop -->
                        <?php while ( have_posts() ) : the_post(); ?>
                            <li <?php post_class('project'); ?> >
                                <span class="title">
                                    <a href="<?php the_permalink(); ?>"
                                       title="<?php the_title_attribute( 'before=Permalink to: "&after="' ); ?>" >
                                        <?php the_title();?>
                                    </a>
                                </span>
                                <span class="body">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                        <?php
                                        if ( has_post_thumbnail() ) {
                                            the_post_thumbnail('thumbnail');
                                        }else{?>
                                            <img src="<?php  echo plugin_dir_url(__FILE__);?>/img/profile_default.png"
                                                 alt=""/>
                                        <?php    }
                                        ?>
                                    </a>

                                </span>
                                <span class="footer">
                                    <?php
                                    $value = get_post_meta( get_the_ID(), 'website-link', true );

                                    if($value){
                                        echo "<a href='".esc_url($value)."'>".esc_html($value)."</a>";
                                    }else{
                                        the_title();
                                    }
                                    ?>
                                </span>
                            </li>
                        <?php endwhile;?>
                    </ul>
                    <div id="pagination">
                        <ul class="pager">
                            <li>  <?php next_posts_link( '&larr; Older posts' ); ?></li>
                            <li><?php previous_posts_link( 'Newer posts &rarr;' ); ?>  </li>
                        </ul>
                    </div>
                <?php else : ?>
                            <p><?php _e( 'Sorry, no posts matched your criteria.', 'cvitae'); ?></p>
                <?php endif; ?>

            </div>
        </div>
        <div class="col-sm-2">
            <?php get_template_part("inc/right_sidebar");?>
        </div>
    </div>
</div>


<?php get_footer();?>
<?php

$projects_fields = array("website-link");
function create_post_type() {
    register_post_type( 'cv_projects',
        array(
            'labels' => array(
                'name' => __( 'Projects', 'cvitaeboost'),
                'singular_name' => __( 'Project', 'cvitaeboost')
            ),
            'public' => true,
            'has_archive' => true,
            'capability_type'    => 'post',
            'menu_position' => 6,
            'menu_icon'=>'dashicons-nametag',
            'supports'=>array('title', 'thumbnail', 'editor'),
            'rewrite' => array('slug' => 'projects'),
        )
    );
}
add_action( 'init', 'create_post_type' );

/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
function create_fields() {
    global $projects_fields;
    $screens = array( 'cv_projects' );

    foreach($projects_fields as $field){
        foreach ( $screens as $screen ) {
            add_meta_box(
                $field,
                sprintf(__( "%1s", 'cvitaeboost' ), $field),
                'field_html',
                $screen,
                'normal',
                'high'
            );
        }

    }
}
add_action( 'add_meta_boxes', 'create_fields' );

/**
 * Prints the box content.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function field_html( $post, $field ) {

    // Add a nonce field so we can check for it later, its like a csrf token
    wp_nonce_field( 'save_'.$field['id'], $field['id'].'_nonce' );

    /*
     * Use get_post_meta() to retrieve an existing value
     * from the database and use the value for the form.
     */
    $value = get_post_meta( $post->ID, $field['id'], true );
    ?>
    <label for="<?php echo $field['id'] ;?>"><?php  _e( 'Enter value here', 'cvitaeboost'); ?> </label>
    <input type="text"
           id="<?php echo $field['id'] ;?>"
           name="<?php echo $field['id'] ;?>"
           value="<?php echo esc_attr($value);?>" size="25" />
<?php    }

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function save_field($post_id){/* OK, its safe for us to save the data now. */
    global $projects_fields;
    foreach($projects_fields as $field){

        /*
         * We need to verify this came from our screen and with proper authorization,
         * because the save_post action can be triggered at other times.
         */
        // Check if our nonce is set.
        if ( ! isset( $_POST[$field.'_nonce'] ) ) {
            return;
        }
        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $_POST[$field.'_nonce'], 'save_'.$field) ) {
            return;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }

        // ensure user can edit this page
        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

        /* OK, its safe for us to save the data now. */
        // Make sure that it is set.
        if (!isset( $_POST[$field] ) ) {
            return;
        }
        $string = sanitize_text_field($_POST[$field]);
        // Update the meta field.
        update_post_meta($post_id, $field, $string);

    }
}
add_action( 'save_post', 'save_field');


function single_work($single_template){
    global $post;
    if ($post->post_type == 'cv_projects') {
        $single_template = dirname( __FILE__ ).'/single-cv_projects.php';
    }
    return $single_template;
}
add_filter('single_template','single_work');

function archive_work($archive_template){
    global $post;
    if ($post->post_type == 'cv_projects') {
        $archive_template = dirname( __FILE__ ).'/archive-cv_projects.php';
    }
    return $archive_template;
}
add_filter('archive_template','archive_work');
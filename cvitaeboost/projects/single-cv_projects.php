<?php get_header();?>

<div class="container">
    <?php get_template_part("inc/menu");?>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-12 ">
            <div class="single">
                <!--            The loop -->
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <div <?php post_class('single_post'); ?> >

                        <div class="single_post_title">
                            <?php the_title('<h2>', '</h2>');?>
                        </div>


                        <div class="clearfix post_meta_data">
                            <ul class="post_meta">

                                <li class="post_date">
                                    <span class="fg_teal"><i class="fa fa-calendar"></i> Date</span>
                                    <?php the_date();?>
                                </li>

                                <li class="bypostauthor">
                                    <span class="fg_teal"><i class="fa fa-user"></i> By</span>
                                    <?php the_author();?>
                                </li>

                                <?php if(get_edit_post_link()){?>
                                    <li class="pull-right edit_post_link">
                                        <a href="<?php echo get_edit_post_link()?>">
                                            <i class="fa fa-edit fg_teal"></i> Edit
                                        </a>
                                    </li>
                                <?php } ?>

                            </ul>
                        </div>

                        <div class="single_post_content">
                            <?php the_content();?>

                            <?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('medium');
                                }
                            ?>
                        </div>

                        <div class="tags_categories">
                            <?php if(has_tag()){?>
                                <div class='post_tags'>
                                    <span class="fg_teal"><i class="fa fa-tags"></i>Tags</span>
                                    <?php the_tags("<span>", ",&nbsp;", "</span>");?>
                                </div>
                            <?php } ?>

                            <?php if(has_category()){?>
                                <div class='post_categories'>
                                    <span class="fg_teal"><i class="fa fa-tags"></i>Categories</span>
                                    <?php the_category(",&nbsp;");?>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="single_post_pagination">
                            <?php wp_link_pages(); ?>
                        </div>
                    </div>

                <?php endwhile; ?>
                    <?php get_template_part("inc/pagination-single")?>
                <?php else :?>

                <p><?php _e( 'Sorry, no posts matched your criteria.', 'cvitae' ); ?></p>

                <?php endif; ?>
            </div>
        </div>
        <div class="col-sm-2">
            <?php get_template_part("inc/right_sidebar");?>
        </div>
    </div>
</div>


<?php get_footer();?>